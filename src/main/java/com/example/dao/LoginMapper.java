package com.example.dao;

import com.example.model.Login;

public interface LoginMapper {
    int deleteByPrimaryKey(Short id);

    int insert(Login record);

    int insertSelective(Login record);

    Login selectByPrimaryKey(Short id);

    int updateByPrimaryKeySelective(Login record);

    int updateByPrimaryKey(Login record);
}