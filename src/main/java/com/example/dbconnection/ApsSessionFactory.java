package com.example.dbconnection;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.example.dao.SaleMapper;
import com.example.model.Sale;


public class ApsSessionFactory {
	//SqlSessionFacory
	private static SqlSessionFactory sqlSessionFactory;

	public static void init() {
		try {
			InputStream inputStream =  Resources.getResourceAsStream("resources/mybatis-config.xml");
			 sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		} catch (Exception e) {
			System.out.println("mybatis-config.xmlファイルが見つかりません。" + e.toString());
		}

	}
	public static SqlSession openSession() {
		init();
		return sqlSessionFactory.openSession();
	}

	public static void main(String[] args) throws IOException {

		try(SqlSession sqlSession = openSession()) {
			// ActorテーブルのMapperを取得します(4)
            SaleMapper map = sqlSession.getMapper(SaleMapper.class);
            // Actorテーブルの主キー（actor_id)が１であるレコードを検索します(5)
            Sale actor = map.selectByPrimaryKey((short) 1);

      // 取得した内容を確認します
            System.out.println("actor.getActorId    = " + actor.getId());
            System.out.println("actor.getFirstName  = " + actor.getItem());
            System.out.println("actor.getLastName   = " + actor.getQuantity());
            System.out.println("actor.getLastUpdate = " + actor.getAmount());

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
