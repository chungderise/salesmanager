package net.codejava;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

class SalesDAOTest {
    private SalesDAO dao;

    @BeforeEach
    void setUp() throws Exception {
        DriverManagerDataSource datasource = new DriverManagerDataSource();
        datasource.setUrl("jdbc:oracle:thin:@192.168.0.104:1521:orcl");
        datasource.setUsername("CHUNGIT24H");
        datasource.setPassword("123456");
        datasource.setDriverClassName("oracle.jdbc.OracleDriver");

        dao = new SalesDAO(new JdbcTemplate(datasource));
    }

    @Test
    void testList() {
        List<Sale> listSale = dao.list();

        assertFalse(listSale.isEmpty());
    }

    @Test
    void testSave() {
        Sale sale = new Sale("Cooler Fan", 1, 49.99f);
        dao.save(sale);
    }

    @Test
    void testGet() {
        int id= 4;
        Sale sale = dao.get(id);
        assertNotNull(sale);
    }

    @Test
    void testUpdate() {
        Sale sale = new Sale();
        sale.setId(4);
        sale.setItem("NokiaProX");
        sale.setQuantity(3);
        sale.setAmount(6666);
        
        dao.update(sale);
    }

    @Test
    void testDelete() {
        int id =1;
        dao.delete(id);
    }

}
